created: 20200916182220032
creator: inmysocks
modified: 20200920123033594
modifier: inmysocks
tags: 
title: Explore Operators

\define makeDisplayFilter() [!is[system]tag[Operator]search{$:/state/operatorsearch}$(categoryTag)$$(dependentTag)$$(useInputTitlesTag)$$(useInputTiddlersTag)$$(useOperandTag)$$(useAllTiddlersTag)$$(useWikiPropertiesTag)$$(useSuffixTag)$$(useInputList)$$(useCategory)$]

\define tagThing() <<tag-pill """$(currentTiddler)$""">>

Save Copy: {{$:/core/ui/Buttons/save-wiki}}

[[How does any of this make sense?]]

[[An important note about the title and list operators]]

[[Some other notes]]

Select different options to see a list of operators that have different properties.

---

Filter by [[fundamental categories|Fundamental Operator Categories]].

<$radio
  tiddler='$:/state/input'
  field='category'
  value=''
>
  All
</$radio>
<$radio
  tiddler='$:/state/input'
  field='category'
  value='contains:category[Construction]'
>
  Construction
</$radio>
<$radio
  tiddler='$:/state/input'
  field='category'
  value='contains:category[Filtering]'
>
  Filtering
</$radio>
<$radio
  tiddler='$:/state/input'
  field='category'
  value='contains:category[Transformation]'
>
  Transformation
</$radio>
<$radio
  tiddler='$:/state/input'
  field='category'
  value='contains:category[Replacement]'
>
  Replacement
</$radio>
<$radio
  tiddler='$:/state/input'
  field='category'
  value='contains:category<ListFiltering>'
>
  List Filter
</$radio>
<$radio
  tiddler='$:/state/input'
  field='category'
  value='contains:category<ListTransform>'
>
  List Transformation
</$radio>
<$radio
  tiddler='$:/state/input'
  field='category'
  value='contains:category<ListReplace>'
>
  List Replace
</$radio>

---

Select [[dependent or independent operators|Dependent vs Independent Operators]], or both

<$radio
  tiddler='$:/state/input'
  field=dependenttag
  value=''
>
  All
</$radio>
<$radio
  tiddler='$:/state/input'
  field=dependenttag
  value='dependent[Independent]'
>
  Independent Operators
</$radio>
<$radio
  tiddler='$:/state/input'
  field=dependenttag
  value="dependent[Don't Use Tiddlers]"
>
  No Tiddlers Used
</$radio>
<$radio
  tiddler='$:/state/input'
  field=dependenttag
  value='dependent[Dependent]'
>
  Dependent Operators
</$radio>

---

Filter by the [[use category of the operator|Operator Use Categories]]

<$radio
  tiddler='$:/state/input'
  field='usecategory'
  value=''
>
  All
</$radio>
<$radio
  tiddler='$:/state/input'
  field='usecategory'
  value='contains:use[String]'
>
  String
</$radio>
<$radio
  tiddler='$:/state/input'
  field='usecategory'
  value='contains:use[Numeric]'
>
  Numeric
</$radio>
<$radio
  tiddler='$:/state/input'
  field='usecategory'
  value='contains:use[Order]'
>
  Order
</$radio>
<$radio
  tiddler='$:/state/input'
  field='usecategory'
  value='contains:use[Listops]'
>
  Listops
</$radio>
<$radio
  tiddler='$:/state/input'
  field='usecategory'
  value='contains:use[Special]'
>
  Special
</$radio>

---

Filter by the types of [[inputs the operator uses|Operator Input Types]]

<$checkbox
  tiddler='$:/state/input'
  field='useinputtitlestag'
  checked='contains:inputs[Input Title]'
  unchecked=''
>
  Input Title
</$checkbox>
<$checkbox
  tiddler='$:/state/input'
  field='useinputtiddlerstag'
  checked='contains:inputs[Input Tiddler]'
  unchecked=''
>
  Input Tiddler
</$checkbox>
<$checkbox
  tiddler='$:/state/input'
  field='usesuffixtag'
  checked='contains:inputs[Suffix]'
  unchecked=''
>
  Suffix
</$checkbox>
<$checkbox
  tiddler='$:/state/input'
  field='useoperandtag'
  checked='contains:inputs[Operand]'
  unchecked=''
>
  Operand
</$checkbox>
<$checkbox
  tiddler='$:/state/input'
  field='usealltiddlerstag'
  checked='contains:inputs[All Tiddlers]'
  unchecked=''
>
  All Tiddlers
</$checkbox>
<$checkbox
  tiddler='$:/state/input'
  field='usewikipropertiestag'
  checked='contains:inputs[Wiki Properties]'
  unchecked=''
>
  Wiki Properties
</$checkbox>
<$checkbox
  tiddler='$:/state/input'
  field='useinputlisttag'
  checked='contains:inputs[Input List]'
  unchecked=''
>
  Input List
</$checkbox>

---

!!Search Operators: <$edit-text tiddler='$:/state/operatorsearch' tag=input placeholder='Enter Search String'/>

<$vars
  categoryTag={{$:/state/input!!category}}
  dependentTag={{$:/state/input!!dependenttag}}
  
  useInputTitlesTag={{$:/state/input!!useinputtitlestag}}
  useInputTiddlersTag={{$:/state/input!!useinputtiddlerstag}}
  useOperandTag={{$:/state/input!!useoperandtag}}
  useAllTiddlersTag={{$:/state/input!!usealltiddlerstag}}
  useWikiPropertiesTag={{$:/state/input!!usewikipropertiestag}}
  useSuffixTag={{$:/state/input!!usesuffixtag}}
  useInputList={{$:/state/input!!useinputlisttag}}

  useCategory={{$:/state/input!!usecategory}}

  ListReplace='List Replace'
  ListFiltering='List Filter'
  ListTransform='List Transform'
>
  <table
    style='width:100%'
  >
    <tr>
      <th>
        `!`
      </th>
      <th>
        Operator
      </th>
      <th>
        Category
      </th>
      <th>
        Dependent?
      </th>
      <th>
        Inputs
      </th>
      <th>
        Use Category
      </th>
    </tr>
    <$list
      filter=<<makeDisplayFilter>>
    >
      <tr>
        <td>
          {{!!negatable}}
        </td>
        <td>
          <$link><<currentTiddler>></$link>
        </td>
        <td>
          <$list filter="[enlist{!!category}]"><<tagThing>></$list>
        </td>
        <td>
          <$list filter="[{!!dependent}]"><<tagThing>></$list>
        </td>
        <td>
          <$list filter="[enlist{!!inputs}]"><<tagThing>></$list>
        </td>
        <td>
          {{!!use}}
        </td>
      </tr>
    </$list>
  </table>
</$vars>